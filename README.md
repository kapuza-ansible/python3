# Python3 install role
## Install
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible python3 -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7;"
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```

### Download this roles
```bash
cat <<'EOF' >> req.yml

- src: git+https://gitlab.com/kapuza-ansible/python3.git
  name: python3
EOF
echo "roles/python3" >> .gitignore
ansible-galaxy install --force -r ./req.yml
```

## Python3 (ubuntu)
```bash
cat << 'EOF' > python3.yml
---
- hosts:
    python3
  become: yes
  become_user: root
  tasks:
    - name: Install python3.6 and pip
      include_role:
        name: python3
      vars:
        install_pip: true
EOF

ansible-playbook python3.yml
```
